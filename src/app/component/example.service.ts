import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ExampleService {
  private sourceSubject$ = new Subject<number>();
  public source$ = this.sourceSubject$.asObservable();

  private anotherSubject$ = new Subject<number>();
  public another$ = this.anotherSubject$.asObservable();
}
